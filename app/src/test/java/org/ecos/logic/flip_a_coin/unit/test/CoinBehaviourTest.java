package org.ecos.logic.flip_a_coin.unit.test;

import static org.ecos.logic.flip_a_coin.model.CoinEnumeration.HEAD;
import static org.ecos.logic.flip_a_coin.model.CoinEnumeration.TAIL;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.greaterThan;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.lessThan;
import static org.hamcrest.Matchers.notNullValue;

import org.ecos.logic.flip_a_coin.model.CoinEnumeration;
import org.ecos.logic.flip_a_coin.model.CoinBehaviour;
import org.junit.jupiter.api.Test;

import java.util.HashMap;
import java.util.Map;

public class CoinBehaviourTest {
    private static final int FLIP_COUNTER = 100000;

    @Test
    void whenIFlipACoinTenThousandTimesTheNumberOfHeadsAndTailsMustBeAlmostTheSame() {
        Map<CoinEnumeration, Long> counter = new HashMap<CoinEnumeration, Long>() {{
            put(HEAD, 0L);
            put(TAIL, 0L);
        }};

        for (int i = 0; i < FLIP_COUNTER; i++) {
            CoinEnumeration coin = CoinBehaviour.coin();

            Long coinCurrentCounter = counter.getOrDefault(coin, 0L);
            assertThat(coinCurrentCounter,is(notNullValue()));
            counter.replace(coin, coinCurrentCounter + 1);
        }

        Long resultHead = counter.getOrDefault(HEAD,0L);
        Long resultTail = counter.getOrDefault(TAIL,0L);

        assertThat(resultHead,is(notNullValue()));
        assertThat(resultTail,is(notNullValue()));

        long middleOfTotalOnePercent = Math.round(FLIP_COUNTER * 0.01);
        assertThat(resultTail,is(greaterThan(resultHead - middleOfTotalOnePercent)));
        assertThat(resultTail,is(lessThan(resultHead + middleOfTotalOnePercent)));


    }
}
