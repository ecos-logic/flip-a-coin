package org.ecos.logic.flip_a_coin.adapter;

import android.view.View;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import org.ecos.logic.flip_a_coin.R;

public class CoinViewHolder extends RecyclerView.ViewHolder {
    private final ImageView imageView;

    public CoinViewHolder(@NonNull View itemView) {
        super(itemView);
        this.imageView = this.itemView.findViewById(R.id.coin_image);
    }

    public void setCoinImage(Integer imageSource) {
        imageView.setImageResource(imageSource);
    }
}
