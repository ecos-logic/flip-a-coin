package org.ecos.logic.flip_a_coin;

import static android.view.View.GONE;
import static android.view.View.VISIBLE;
import static org.ecos.logic.flip_a_coin.R.drawable.banking_head;
import static org.ecos.logic.flip_a_coin.R.drawable.banking_tail;
import static org.ecos.logic.flip_a_coin.R.drawable.cent_head;
import static org.ecos.logic.flip_a_coin.R.drawable.cent_tail;
import static org.ecos.logic.flip_a_coin.R.drawable.coin_one_gold;
import static org.ecos.logic.flip_a_coin.R.drawable.coin_one_silver;
import static org.ecos.logic.flip_a_coin.R.drawable.gold_coin;
import static org.ecos.logic.flip_a_coin.R.drawable.middle_east_coin_head;
import static org.ecos.logic.flip_a_coin.R.drawable.middle_east_coin_tail;
import static org.ecos.logic.flip_a_coin.R.drawable.non_cicle_coin_head;
import static org.ecos.logic.flip_a_coin.R.drawable.non_cicle_coin_tail;
import static org.ecos.logic.flip_a_coin.R.drawable.penny_head;
import static org.ecos.logic.flip_a_coin.R.drawable.penny_tail;
import static org.ecos.logic.flip_a_coin.R.drawable.silver_coin;
import static org.ecos.logic.flip_a_coin.R.string.head;
import static org.ecos.logic.flip_a_coin.R.string.tail;
import static org.ecos.logic.flip_a_coin.model.CoinEnumeration.HEAD;
import static org.ecos.logic.flip_a_coin.presenter.FlipACoinPresenter.HEAD_STRING_VALUE;
import static org.ecos.logic.flip_a_coin.presenter.FlipACoinPresenter.TAIL_STRING_VALUE;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.animation.ValueAnimator;
import android.os.Bundle;
import android.view.View;
import android.view.animation.BounceInterpolator;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.content.res.AppCompatResources;
import androidx.databinding.DataBindingUtil;

import com.jackandphantom.carouselrecyclerview.CarouselLayoutManager;
import com.jackandphantom.carouselrecyclerview.CarouselRecyclerview;

import org.ecos.logic.flip_a_coin.adapter.CoinAdapter;
import org.ecos.logic.flip_a_coin.databinding.ActivityMainBinding;
import org.ecos.logic.flip_a_coin.model.CoinBehaviour;
import org.ecos.logic.flip_a_coin.model.CoinEnumeration;
import org.ecos.logic.flip_a_coin.model.CoinValues;
import org.ecos.logic.flip_a_coin.model.HeadAndTailReference;
import org.ecos.logic.flip_a_coin.presenter.FlipACoinContract;
import org.ecos.logic.flip_a_coin.presenter.FlipACoinPresenter;

import java.util.ArrayList;
import java.util.Arrays;

public class MainActivity extends AppCompatActivity implements FlipACoinContract.FlipACoinView {
    public static final String TAG = "tag";
    public static final String SCALE_X = "scaleX";
    public static final String SCALE_Y = "scaleY";
    public static final String TRANSLATION_Y = "translationY";
    public static final String ROTATION_X = "rotationX";
    public static final int ANIMATION_DURATION = 3000;
    private final ArrayList<HeadAndTailReference> headsAndTails;
    private boolean mustChange = true;
    private boolean mustFlip = true;
    private final CoinValues coinValues;
    private View flipAction;
    private FlipACoinPresenter mainActivityPresenter;
    private AnimatorSet animatorSet;

    private ImageView coin;
    private TextView coinLabel;
    private CarouselRecyclerview carouselRecyclerview;


    public MainActivity() {
        headsAndTails = new ArrayList<>(
            Arrays.asList(
                new HeadAndTailReference(banking_head,banking_tail),
                new HeadAndTailReference(gold_coin,silver_coin),
                new HeadAndTailReference(penny_head,penny_tail),
                new HeadAndTailReference(middle_east_coin_head,middle_east_coin_tail),
                new HeadAndTailReference(non_cicle_coin_head,non_cicle_coin_tail),
                new HeadAndTailReference(cent_head,cent_tail),
                new HeadAndTailReference(coin_one_gold,coin_one_silver)
            )
        );
        this.coinValues = new CoinValues(headsAndTails,HEAD_STRING_VALUE,TAIL_STRING_VALUE, head,tail);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        String coinTag = manageAppState(savedInstanceState);

        ActivityMainBinding binding = DataBindingUtil.setContentView(this, R.layout.activity_main);
        this.mainActivityPresenter = new FlipACoinPresenter(this);
        binding.setPresenter(this.mainActivityPresenter);
        this.setContentView(binding.getRoot());

        this.coin = binding.coin;
        this.flipAction = binding.flipAction;
        this.coinLabel = binding.coinLabel;

        this.animationSetup();

        this.mainActivityPresenter.showCoinAs(coinTag);

        CoinAdapter adapter = new CoinAdapter(this.headsAndTails);

        this.carouselRecyclerview = this.findViewById(R.id.coin_carrousel);
        this.carouselRecyclerview.setAdapter(adapter);
        this.carouselRecyclerview.set3DItem(true);
        this.carouselRecyclerview.setInfinite(false);
        this.carouselRecyclerview.setItemSelectListener(this.manageSelection);
    }

    @Override
    protected void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);

        outState.putString(TAG, this.coin.getTag().toString());
    }

    @Override
    public void onFlipAction() {
        animatorSet.start();
    }

    @Override
    public void disablingFlipAction() {
        this.flipAction.setEnabled(false);
    }

    @Override
    public void setCoinTo(CoinEnumeration coinEnumeration) {
        this.coin.setImageDrawable(AppCompatResources.getDrawable(this, this.coinValues.getCoinIdDrawables(this.mainActivityPresenter.geCoinIndex(),coinEnumeration)));
        this.coin.setTag(this.coinValues.getTag(coinEnumeration));
        String coinValueAsString = this.getResources().getString(this.coinValues.getHeadContentIdString(coinEnumeration));
        this.coin.setContentDescription(coinValueAsString);
        this.coinLabel.setText(coinValueAsString);
    }

    @Override
    public void enablingFlipAction() {
        this.flipAction.setEnabled(true);
    }

    @Override
    public void hideCoinLabel() {
        this.coinLabel.setVisibility(GONE);
    }

    @Override
    public void showCoinLabel() {
        this.coinLabel.setVisibility(VISIBLE);
    }

    @Override
    public void hideCoinSelectors() {
        this.carouselRecyclerview.setVisibility(GONE);
    }

    @Override
    public void showCoinSelectors() {
        this.carouselRecyclerview.setVisibility(VISIBLE);
    }

    @Override
    public void changeCoinUsing() {
        this.hideCoinLabel();
        this.setCoinTo(HEAD);
    }


    private static String manageAppState(Bundle savedInstanceState) {
        String coinTag = HEAD_STRING_VALUE;
        if (savedInstanceState != null) {
            coinTag = savedInstanceState.getString(TAG);
            if (coinTag == null || coinTag.isEmpty())
                coinTag = HEAD_STRING_VALUE;
        }
        return coinTag;
    }

    private void animationSetup() {
        this.animatorSet = new AnimatorSet();
        this.animatorSet.setDuration(ANIMATION_DURATION);
        this.animatorSet.setInterpolator(new BounceInterpolator());
        this.animatorSet.addListener(this.animationListenerStartEnd);

        ObjectAnimator scaleX = ObjectAnimator.ofFloat(this.coin, SCALE_X, 1f, 1.4f, 1f);
        ObjectAnimator scaleY = ObjectAnimator.ofFloat(this.coin, SCALE_Y, 1f, 1.4f, 1f);

        ObjectAnimator translationY = ObjectAnimator.ofFloat(this.coin, TRANSLATION_Y, 1, -400f, 1);

        ObjectAnimator rotateAnimation = ObjectAnimator.ofFloat(this.coin, ROTATION_X, 0f, 360f * 4);

        rotateAnimation.addUpdateListener(this.animationUpdateListener);

        this.animatorSet.
            play(rotateAnimation).
            with(scaleY).with(scaleX).
            with(translationY);
    }

    private final Animator.AnimatorListener animationListenerStartEnd = new AnimatorListenerAdapter() {
        @Override
        public void onAnimationStart(Animator animation) {
            MainActivity.this.mustFlip = true;
            MainActivity.this.mainActivityPresenter.animationStart();
        }

        @Override
        public void onAnimationEnd(Animator animation) {
            MainActivity.this.mainActivityPresenter.animationEnds();
        }

    };

    private final ValueAnimator.AnimatorUpdateListener animationUpdateListener = animation -> {
        float rotationX = MainActivity.this.coin.getRotationX();

        if (rotationX % 180 >= 90)  {
            if(MainActivity.this.mustFlip) {
                MainActivity.this.setCoinTo(CoinBehaviour.coin());
                MainActivity.this.mustFlip = false;
            }

            if (MainActivity.this.mustChange) {
                MainActivity.this.mainActivityPresenter.flipCoinTo(MainActivity.this.coin.getTag().toString());
                MainActivity.this.mustChange = false;
            }
        } else
            MainActivity.this.mustChange = true;
    };

    private final CarouselLayoutManager.OnSelected manageSelection = new CarouselLayoutManager.OnSelected() {
        @Override
        public void onItemSelected(int position) {
            mainActivityPresenter.selectCoin(position);
        }
    };

}