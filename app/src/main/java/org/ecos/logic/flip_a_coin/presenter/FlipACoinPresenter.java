package org.ecos.logic.flip_a_coin.presenter;

import org.ecos.logic.flip_a_coin.model.CoinEnumeration;

public class FlipACoinPresenter implements FlipACoinContract.Presenter {
    public static final String HEAD_STRING_VALUE = "head";
    public static final String TAIL_STRING_VALUE = "tail";
    private final FlipACoinContract.FlipACoinView flipACoinView;
    private Integer coinIndex = 0;

    public FlipACoinPresenter(FlipACoinContract.FlipACoinView flipACoinView) {
        this.flipACoinView = flipACoinView;
    }

    @Override
    public void flipACoin() {
        this.flipACoinView.onFlipAction();
        this.flipACoinView.hideCoinSelectors();
    }

    @Override
    public void animationStart() {
        this.flipACoinView.disablingFlipAction();
        this.flipACoinView.hideCoinLabel();
    }

    @Override
    public void animationEnds() {
        this.flipACoinView.enablingFlipAction();
        this.flipACoinView.showCoinLabel();
        this.flipACoinView.showCoinSelectors();
    }

    @Override
    public void flipCoinTo(String headOrTail) {
        dynamicShowCoinAs(headOrTail, CoinEnumeration.HEAD, CoinEnumeration.TAIL);
    }

    @Override
    public void showCoinAs(String headOrTail) {
        dynamicShowCoinAs(headOrTail, CoinEnumeration.TAIL, CoinEnumeration.HEAD);
    }

    @Override
    public int geCoinIndex() {
        return this.coinIndex;
    }

    @Override
    public void selectCoin(int position) {
        this.coinIndex = position;

        this.flipACoinView.changeCoinUsing();
    }

    private void dynamicShowCoinAs(String headOrTail, CoinEnumeration defaultValue, CoinEnumeration valueIfHeadOrTailIsHeadString) {
        CoinEnumeration newCoinValue = defaultValue;
        if (headOrTail.equals(HEAD_STRING_VALUE))
            newCoinValue = valueIfHeadOrTailIsHeadString;

        this.showCoinAs(newCoinValue);
    }

    private void showCoinAs(CoinEnumeration coin) {
        this.flipACoinView.setCoinTo(coin);
    }

}
