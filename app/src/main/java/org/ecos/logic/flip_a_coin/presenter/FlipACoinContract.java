package org.ecos.logic.flip_a_coin.presenter;

import org.ecos.logic.flip_a_coin.model.CoinEnumeration;

public interface FlipACoinContract {
    interface Presenter {
        void flipACoin();

        void animationStart();
        void animationEnds();

        void flipCoinTo(String coinEnumeration);

        void showCoinAs(String coinTag);

        int geCoinIndex();

        void selectCoin(int position);
    }
    interface FlipACoinView {
        void onFlipAction();

        void enablingFlipAction();

        void disablingFlipAction();

        void setCoinTo(CoinEnumeration coinEnumeration);

        void hideCoinLabel();

        void showCoinLabel();

        void hideCoinSelectors();

        void showCoinSelectors();

        void changeCoinUsing();
    }
}