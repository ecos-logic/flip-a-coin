package org.ecos.logic.flip_a_coin.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import org.ecos.logic.flip_a_coin.R;
import org.ecos.logic.flip_a_coin.model.CoinEnumeration;
import org.ecos.logic.flip_a_coin.model.HeadAndTailReference;

import java.util.ArrayList;

public class CoinAdapter extends RecyclerView.Adapter<CoinViewHolder> {
    private final ArrayList<HeadAndTailReference> headsAndTails;

    public CoinAdapter(ArrayList<HeadAndTailReference> headsAndTails) {
        this.headsAndTails = headsAndTails;
    }

    @NonNull
    @Override
    public CoinViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.cell, parent, false);
        return new CoinViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull CoinViewHolder holder, int position) {
        holder.setCoinImage(this.headsAndTails.get(position).getReferenceBy(CoinEnumeration.HEAD));
    }

    @Override
    public int getItemCount() {
        return this.headsAndTails.size();
    }
}
