package org.ecos.logic.flip_a_coin.model;

import static org.ecos.logic.flip_a_coin.model.CoinEnumeration.HEAD;
import static org.ecos.logic.flip_a_coin.model.CoinEnumeration.TAIL;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CoinValues {
    List<HeadAndTailReference> selectableCoinIdDrawables;
    Map<CoinEnumeration, String> tagMap = new HashMap<>();
    Map<CoinEnumeration, Integer> contentIdStringMap = new HashMap<>();

    public CoinValues(List<HeadAndTailReference> selectableCoinIdDrawables, String headTag, String tailTag, int headContentIdStringMap, int tailContentIdString) {
        this.selectableCoinIdDrawables = selectableCoinIdDrawables;

        this.tagMap.put(HEAD, headTag);
        this.tagMap.put(TAIL, tailTag);

        this.contentIdStringMap.put(HEAD, headContentIdStringMap);
        this.contentIdStringMap.put(TAIL, tailContentIdString);
    }

    public int getCoinIdDrawables(int selectableCoinIndex, CoinEnumeration coinEnumeration) {
        return this.selectableCoinIdDrawables.get(selectableCoinIndex).getReferenceBy(coinEnumeration);
    }

    public String getTag(CoinEnumeration coinEnumeration) {
        return this.tagMap.get(coinEnumeration);
    }

    public Integer getHeadContentIdString(CoinEnumeration coinEnumeration) {
        return this.contentIdStringMap.get(coinEnumeration);
    }

    public Integer getCoinIdDrawablesLength() {
        return this.selectableCoinIdDrawables.size();
    }
}
