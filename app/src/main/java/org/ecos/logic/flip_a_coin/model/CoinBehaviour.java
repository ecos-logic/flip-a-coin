package org.ecos.logic.flip_a_coin.model;

import java.util.Random;

public class CoinBehaviour {
    public static CoinEnumeration coin(){
        Random random = new Random();

        if(random.nextInt(2) == 0)
            return CoinEnumeration.HEAD;
        else
            return CoinEnumeration.TAIL;
    }
}