package org.ecos.logic.flip_a_coin.model;

import static org.ecos.logic.flip_a_coin.model.CoinEnumeration.HEAD;
import static org.ecos.logic.flip_a_coin.model.CoinEnumeration.TAIL;

import java.util.HashMap;
import java.util.Map;

public class HeadAndTailReference {
    private final Map<CoinEnumeration,Integer> headReferenceMap = new HashMap<>();

    public HeadAndTailReference(Integer headReference, Integer tailReference) {
        this.headReferenceMap.put(HEAD,headReference);
        this.headReferenceMap.put(TAIL,tailReference);
    }

    public Integer getReferenceBy(CoinEnumeration coinEnumeration) {
        return this.headReferenceMap.get(coinEnumeration);
    }
}
