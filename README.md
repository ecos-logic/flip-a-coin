## Flip a coin:

![Status](https://gitlab.com/ecos-logic/flip-a-coin/badges/develop/pipeline.svg)

Game to flip a coin you can:
* Flip the coin
* Change between landscape and portrait
* Select between different coins

[<img src="https://fdroid.gitlab.io/artwork/badge/get-it-on.png"
     alt="Get it on F-Droid"
     height="80">](https://f-droid.org/packages/org.ecos.logic.flip_a_coin/)

Or download the latest APK from the [Releases Section](https://gitlab.com/ecos-logic/flip-a-coin/-/releases).

## Donations
If you feel like it…

[<img src="https://www.paypalobjects.com/webstatic/de_DE/i/de-pp-logo-100px.png" border="0"
    alt="PayPal Logo">](https://www.paypal.com/paypalme/PVergaraCastro)
